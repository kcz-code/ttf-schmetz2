#!/bin/sh
echo "ng-serve : launch ttf-schmetz application"
if [ ! -d "/opt/project/src" ]; then
  echo ""
  echo "empty project? please run: \"ng new ttf-schmetz --directory .\""
fi
echo "PS1='ttf-schmetz: '" >> ~/.profile
exec sh -i -l
