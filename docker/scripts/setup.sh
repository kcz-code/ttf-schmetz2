#!/bin/sh
if [ -z "$LOCAL_USER_ID" ]; then
  echo "LOCAL_USER_ID not set!"
  exit 1
fi
if [ -z "$LOCAL_USER_GROUP" ]; then
  echo "LOCAL_USER_GROUP not set!"
  exit 1
fi

echo "developer:x:${LOCAL_USER_ID}:${LOCAL_USER_GROUP}:Developer,,,:/home/developer:/bin/sh" >> /etc/passwd
echo "developer:x:${LOCAL_USER_GROUP}:" >> /etc/group
echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer
chmod 0440 /etc/sudoers.d/developer
mkdir -p /home/developer
chown developer:developer /home/developer

gosu developer sh /tmp/s/start.sh
